var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    tc: {
        type: String,
        required: true
    },
    role: {
        type: Number,
        required: true
    }
})

module.exports = UserSchema;