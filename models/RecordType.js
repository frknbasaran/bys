var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;
    
var RecordTypeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    fields: [
        {
            "name":String,
            "type":String
        }
    ]
});

module.exports = RecordTypeSchema;