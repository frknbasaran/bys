var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;
    
var RecordSchema = new Schema({
    type: {
        type: Schema.Types.ObjectId,
        ref: 'RecordType'
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    authorized: [
        {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    data : [{
        "key": String,
        "value":String
    }]
    
})

module.exports = RecordSchema;