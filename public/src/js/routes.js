'use strict';

/**
 * Route configuration for the RDash module.
 */
angular.module('RDash', ['ui.bootstrap', 'ui.router', 'ngNotify', 'angular-loading-bar', 'ngCookies', 'angularMoment'])

    .run(['$rootScope', '$cookieStore', '$location', 'UserService', '$http', '$state', 'ngNotify',
        function ($rootScope, $cookieStore, $location, UserService, $http, $state, ngNotify) {

            ngNotify.config({
                theme: 'pure',
                position: 'bottom',
                duration: 1000,
                type: 'info',
                sticky: false,
                html: false
            });
            
            var host = window.location.host;
            
            $rootScope.serviceUrl = "http://" + host + "/api/";
            $rootScope.globals = $cookieStore.get("globals") || {};

            if ($rootScope.globals.currentUser) {
                $http.defaults.headers.common["user"] = $rootScope.globals.currentUser._id;
                $http.defaults.headers.common["token"] = $rootScope.globals.currentUser.token;
            }

            $rootScope.$on('$locationChangeStart', function () {
                // Check for user is signed in or not
                
                if ($location.path() != "/giris" && !$rootScope.globals.currentUser) {
                    $location.path("/giris");
                }
                
                if ($location.path() == "/giris" && $rootScope.globals.currentUser) {
                    $location.path("/");
                }
                
            });


        }
    ])

.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
            .state('index', {
                url: '/',
                views: {
                    "header": {
                        templateUrl: 'templates/header.html'
                        //controller: 'HeaderController'
                    },
                    "sidebar": {
                        templateUrl: 'templates/sidebar.html'
                        
                    },
                    "content": {
                        templateUrl: 'templates/dashboard.html'
                    }
                }
                
            })
            .state('tables', {
                url: '/tables',
                templateUrl: 'templates/tables.html'
            })
            .state('login', {
                url: '/giris?identity&username',
                views: {
                    "full": {
                        templateUrl: 'templates/login.html',
                        controller: 'LoginController'
                    }
                }
            })
            .state('logoff', {
                url: '/cikis',
                views: {
                    "full": {
                        templateUrl: "templates/login.html",
                        controller: "LoginController"
                    }
                }
            })
    }
]);