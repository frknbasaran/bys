angular.module('RDash')
    .controller('LoginController', ['$scope', '$http', '$cookieStore', '$location', '$rootScope', '$state', 'UserService', 'ngNotify', LoginController]);

function LoginController($scope, $http, $cookieStore, $location, $rootScope, $state, UserService, ngNotify) {
    
    $scope.credentials = {"tc":"","password":"","username":""};
    
    if (typeof $state.params.identity !== "undefined") {
        $scope.credentials.tc = $state.params.identity;
    }
    
    if (typeof $state.params.username !== "undefined") {
        $scope.credentials.username = $state.params.username;
    }
    
    if (typeof $state.params.username === "undefined" || typeof $state.params.identity === "undefined") {
        $scope.error = "Sisteme giriş yetkiniz yok. Lütfen sistem yöneticisine danışın ya da portaldan bağlanmayı deneyin.";
    }
    
    $scope.logIn = function() {
        console.log("fired!")
        UserService
            .logIn($scope.credentials.tc, $scope.credentials.password, function (response) {
                if (response.error) ngNotify.set(response.message, 'error');
                else {
                    UserService.setCredentials(response.data.user, response.data.token);
                    $cookieStore.put('globals', $rootScope.globals);

                    // Setting the request headers which are added all requests by automatically
                    $http.defaults.headers.common["user"] = $rootScope.globals.currentUser._id;
                    $http.defaults.headers.common["token"] = $rootScope.globals.currentUser.token;

                    $location.path('/');
                }
            })
    }
    
    if($state.is('logoff')) {

        UserService
            .logOut(function(response) {
                if (!response.error) {
                  $cookieStore.put('globals', {});  
                } else console.error("LoginController->logOut():" + response.message)
            })

    }

}