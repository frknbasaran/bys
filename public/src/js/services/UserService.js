angular.module('RDash')
    .factory('UserService', ['$http', '$rootScope', function ($http, $rootScope) {

        var service = {}

       service.logIn = function (tc, password, callback) {

            $http({
                method: 'POST',
                url: $rootScope.serviceUrl + "users/login",
                data: "tc=" + tc + "&password=" + password,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
                .then(function successCallback(response) {
                    callback(response.data);
                })

        }

        service.logOut = function (callback) {

            $http({
                method: 'POST',
                url: $rootScope.serviceUrl + "users/logout"
            })
                .then(function successCallback(response) {
                    callback(response.data);
                })
                
        }

        service.setCredentials = function (user, token) {
            $rootScope.globals = {
                currentUser: {
                    _id: user._id,
                    name: user.name,
                    role: user.role,
                    token: token.token
                }
            };
        }
        
        return service;
        
    }]);