var promise = require('bluebird');
var md5 = require('md5');
var TokenSchema = require('../models/Token');
var soap = require('soap');


module.exports = function (M, mongoose) {

    return {

        // TODO: Bu fonksiyonu parçalara ayır bu nasıl fonksiyon lan böyle 136 satır
        "logIn": function (tc, license, password, ip) {

            var deferred = promise.pending();

            var url = 'http://srv.polisoft.com.tr:1238/new_pany/psAdmin.dll/wsdl/IpsAdmin';

            var args = {pTCKN: tc, pLisansNo: license, pSifre: password, pIPAdresi: ip};

            soap.createClient(url, function (err, client) {

                client.doPortalLogin(args, function (err, result) {

                    if (err) deferred.reject(err);
                    else {

                        if (result.return.HataKodu.$value == 0) {

                            M
                                .find({tc: tc})
                                .exec(function (err, user) {
                                    /*
                                     Servisten onay geldiyse VE Kullanıcı kaydı BYS'de var ise
                                     */
                                    if (!err && user.length > 0) {

                                        if (user[0].role == result.return.KullaniciTipi.$value) {

                                            var Token = mongoose.model('Token', TokenSchema);
                                            
                                            Token
                                                .find()
                                                .where('user', user[0]._id)
                                                .exec(function (err, tokens) {
                                                    /*
                                                     Kullanıcının geçerli bir token'ı mevcut ise
                                                     */
                                                    if (!err && tokens.length > 0) {

                                                        deferred.fulfill({
                                                            "error": false,
                                                            "data": {
                                                                "user": user[0]._id,
                                                                "token": tokens[0]
                                                            }
                                                        })

                                                    }
                                                    /*
                                                     Kullanıcını geçerli bir Token'ı mevcut DEĞİL ise
                                                     */
                                                    else {

                                                        var uid = require('rand-token').uid;
                                                        var tokenString = uid(16);

                                                        var generatedToken = new Token();

                                                        generatedToken.token = tokenString;
                                                        generatedToken.user = user[0]._id;

                                                        generatedToken.save(function (err, token) {

                                                            if (!err) {

                                                                deferred.fulfill({
                                                                    "error": false,
                                                                    "data": {
                                                                        "user": user[0]._id,
                                                                        "token": token
                                                                    }
                                                                })

                                                            }
                                                            else deferred.reject(err)
                                                        })
                                                    }
                                                });
                                        }
                                        else {
                                            
                                            var Token = mongoose.model('Token', TokenSchema);

                                            user[0].role = result.return.KullaniciTipi.$value;

                                            user[0].save(function (err, savedUser) {
                                                if (err) deferred.reject(err);
                                                else {

                                                    Token
                                                        .find()
                                                        .where('_id', savedUser._id)
                                                        .exec(function (err, tokens) {
                                                            /*
                                                             Kullanıcının geçerli bir token'ı mevcut ise
                                                             */
                                                            if (!err && tokens.length > 0) {

                                                                deferred.fulfill({
                                                                    "error": false,
                                                                    "data": {
                                                                        "user": savedUser._id,
                                                                        "token": tokens[0]
                                                                    }
                                                                })

                                                            }
                                                            /*
                                                             Kullanıcını geçerli bir Token'ı mevcut DEĞİL ise
                                                             */
                                                            else {

                                                                var uid = require('rand-token').uid;
                                                                var tokenString = uid(16);

                                                                var generatedToken = new Token();

                                                                generatedToken.token = tokenString;
                                                                generatedToken.user = user[0]._id;

                                                                generatedToken.save(function (err, token) {

                                                                    if (!err) {

                                                                        deferred.fulfill({
                                                                            "error": false,
                                                                            "data": {
                                                                                "user": savedUser._id,
                                                                                "token": token
                                                                            }
                                                                        })

                                                                    }
                                                                    else deferred.reject(err)
                                                                })
                                                            }
                                                        });

                                                }

                                            })
                                        }

                                    }
                                    /*
                                     Servisten onay geldiyse VE Kullanıcı kaydı portalda henüz oluşturulmamış ise
                                     */
                                    else {

                                        var Token = mongoose.model('Token', TokenSchema);

                                        var temporary = new M({
                                            "name": result.return.AdiSoyadi.$value,
                                            "role": result.return.KullaniciTipi.$value,
                                            "tc": tc
                                        })

                                        temporary
                                            .save(function (err, savedUser) {
                                                if (!err) {

                                                    var uid = require('rand-token').uid;
                                                    var tokenString = uid(16);
                                                    var generatedToken = new Token();

                                                    generatedToken.token = tokenString;
                                                    generatedToken.user = savedUser._id;

                                                    generatedToken.save(function (err, token) {

                                                        if (!err) {
                                                            deferred.fulfill({
                                                                "error": false,
                                                                "data": {
                                                                    "user": savedUser._id,
                                                                    "token": token
                                                                }
                                                            })
                                                        }
                                                        else deferred.reject(err)
                                                    })
                                                }
                                            })
                                    }
                                });
                        } else {
                            deferred.fulfill({"error": true, "message": result.return.HataMesaji.$value});
                        }
                    }
                });
            });

            return deferred.promise;
        },

        "logOff": function (token) {

            console.log("Core:User->logOff(" + token + ")")

            var deferred = promise.pending();

            var Token = mongoose.model('Token', TokenSchema);

            Token
                .findOne({"token": token})
                .remove()
                .exec(function (err) {
                    if (!err) deferred.fulfill(true)
                    else deferred.reject({"error": true, "message": "Token'da bir hata var"})
                })

            return deferred.promise;
        },
        
        "getAuthByToken": function (token) {

            console.log("Core:User->getAuthByToken(" + token + ")")

            var deferred = promise.pending();

            var Token = mongoose.model('Token', TokenSchema);

            Token
                .findOne({"token": token})
                .exec(function (err, token) {
                    if (!err && token != null) {
                        M
                            .findOne({_id: token.user})
                            .exec(function (err, user) {
                                if (!err) deferred.fulfill({"error": false, "data": user});
                                else deferred.reject({"error": true, "message": err.message})
                            })
                    }
                    else deferred.reject({"error": true, "message": "Your token was expired or incorrect"})
                })


            return deferred.promise;

        }
    }
}