var mongoose = require('mongoose');

var TokenSchema = require('../models/Token');
var UserSchema = require('../models/User');

module.exports = {

    "isAdmin": function (req, res, next) {

        var TokenModel = mongoose.model('Token', TokenSchema);
        var UserModel = mongoose.model('User', UserSchema);

        TokenModel
            .findOne({token: req.headers.ptoken})
            .exec(function (err, token) {
                if (err) res.json(err);
                else if(token == null) res.sendStatus(403);
                else {
                    UserModel
                        .findOne({_id: token.user})
                        .exec(function (err, user) {
                            if (err) res.json(err);
                            else {
                                if (user.role.env == 1 && user.role.envAuth == 0) next();
                                else res.sendStatus(403);
                            }
                        })
                }
            })

    },

    "isLoggedOn": function (req, res, next) {

        var TokenModel = mongoose.model('Token', TokenSchema);

        TokenModel
            .findOne({token: req.headers.ptoken})
            .exec(function (err, token) {
                if (err) res.json(err);
                else if(token == null) res.sendStatus(403);
                else {
                    if (token.user == req.headers.user) next();
                    else res.sendStatus(403);
                }
            })

    }
}