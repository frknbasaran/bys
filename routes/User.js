var UserSchema = require('../models/User');
var Auth       = require('../core/Auth');
var TokenSchema= require('../models/Token');

var LoginRouter = function (app, restful, mongoose) {
    
    var UserResource = app.user = restful.model('User', UserSchema)
        .methods(['get','post']);
    
    var UserLogic = require('../core/User')(UserResource, mongoose);
    
    UserResource
        .route('login.post', function (req, res) {
            UserLogic
                .logIn(req.body.tc, "169832", req.body.password, req.ip)
                .then(function (result) {
                    res.json(result);
                })
                .catch(function (err) {
                    res.json({"error": true, "message": "Sunucu Hatası:" + err.message});
                })
        })
    
    UserResource
        .route('logout.post', function (req, res) {
        
            UserLogic
                .logOff(req.headers.token, req.headers.user)
                .then(function(result) {
                    if (result) res.json({"error":false, "message":"session destroyed successfully"});
                    else res.json({"error":true, "message": "your credentials incorrect"})
                })
                .catch(function(err) {
                    res.json({"error": true, "message": err})
                })
        })
    
    UserResource.register(app, '/api/users');
}

module.exports = LoginRouter;