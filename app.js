var express = require('express');
var path = require('path');
var mongoose = require('mongoose');
var restful = require('node-restful');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config');

var app = express();

mongoose.connect(config.database.connectionString);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public/dist')));

var UserRouter = require('./routes/User')(app, restful, mongoose);



module.exports = app;
